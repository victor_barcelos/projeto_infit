import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testeteladelogin/Model/metodosTela.dart';
import 'package:testeteladelogin/services/auth_service.dart';

class InFITCadastro extends StatefulWidget {
  const InFITCadastro({ Key? key }) : super(key: key);

  @override
  _InFITCadastroState createState() => _InFITCadastroState();
}

class _InFITCadastroState extends State<InFITCadastro> {

  final formKey = GlobalKey<FormState>();
  final email = TextEditingController();
  final senha = TextEditingController();
  final nome = TextEditingController();
  final altura = TextEditingController();
  final peso = TextEditingController();
  final idade = TextEditingController();

  bool estaLogado = true;
  bool checkado = false;
  late String titulo; // tema da pagina
  late String botao1; // botao de concluir
  late String botao2; // botao de revogar

  @override
  void initState() {
    super.initState();
    setFormAction(true);
  }

  setFormAction(bool acionar) {
    setState(() {
      estaLogado = acionar;
      if (!estaLogado) { // caso tenha conta
        Navigator.pushNamed(context, "login");
      }
    });
  }

  cadastrar() async {
    try {
      await context.read<AuthService>().cadastrar(email.text, senha.text, );
    } 
    on AutenticarException catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.mensagem)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(corFundo),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 100, right: 40, left: 40,),
          child: Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                caixaDeTexto("Cadastro - InFIT", corPrimaria, 30, TextAlign.center), pularLinha(40),
                TextFormField(
                  controller: nome,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Nome",
                  ),
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Informe seu nome";
                    }
                    return null;
                  },
                ), pularLinha(10),
                TextFormField(
                  controller: email,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Email",
                  ),
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Informe o email corretamente";
                    }
                    return null;
                  },
                ), pularLinha(10),
                TextFormField(
                  controller: senha,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Senha",
                  ),
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "Informe sua senha";
                    }
                    else if (value.length < 6) {
                      return "Sua senha deve possuir no mínimo 6 caracteres";
                    }
                    return null;
                  },
                ), pularLinha(10),
                TextFormField(
                  controller: altura,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Altura",
                  ),
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    double alturaNumero = double.parse(altura.text);
                    if (value == null || value.isEmpty) {
                      return "Informe sua Altura";
                    }
                    else if (alturaNumero < 54 || alturaNumero > 272) {
                      return "Insira uma altura válida";
                    }
                    return null;
                  },
                ), pularLinha(10),
                TextFormField(
                  controller: peso,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Peso",
                  ),
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    double pesoNumero = double.parse(peso.text);
                    if (value == null || value.isEmpty) {
                      return "Informe seu peso";
                    }
                    else if (pesoNumero < 2 || pesoNumero > 635) {
                      return "Insira um peso válido";
                    }
                    return null;
                  },
                ), pularLinha(20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                      child: Text("Retornar"),
                      onPressed: () {
                        Navigator.pushNamed(context, "login");
                      }, 
                    ),
                    ElevatedButton(
                      child: Text("Concluir"),
                      onPressed: () {
                        if (formKey.currentState!.validate()) {
                          cadastrar();
                        }
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}



















/*
class TelaCadastro extends StatefulWidget {

  int id = 0;
  String email = "";
  String nome = "";
  double altura = 0.0;
  double peso = 0.0;
  String senha = "";
  String objetivos = "";
  String pcd = "";

  //Date datanascimento;

  /*TelaCadastro({this.id, this.email, this.altura, this.peso, this.objetivos, this.pcd});

  List<TelaCadastro> objetivosExercicios = [
    TelaCadastro(id: 1, objetivos: "crescimento", pcd: "2"),
    TelaCadastro(id: 2, objetivos: "emagrecer", pcd: "3"),
    TelaCadastro(id: 3, objetivos: "ganho muscular", pcd: "4"),
    TelaCadastro(id: 4, objetivos: "resistencia", pcd: "5"),
  ];*/

  @override
  _TelaCadastroState createState() => _TelaCadastroState();
}

class _TelaCadastroState extends State<TelaCadastro> {

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(corFundo),
      body: Padding(
        padding: EdgeInsets.only(top: 40, right: 40, left: 40,),
        child: Form(
          key: _formKey,
            child: ListView(
              children: <Widget>[
                caixaDeTexto("Cadastro - InFIT", corPrimaria, 2 * tamanhoLetra, TextAlign.center), pularLinha(60),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    preenchimentoUsuario("E-mail", TextInputType.emailAddress, "Insira um E-mail.", "Insira um email válido", widget.email), pularLinha(20),
                    preenchimentoUsuario("Nome", TextInputType.text, "Insira um nome.", "Insira um nome com pelo menos 3 caracteres", widget.nome), pularLinha(20),
                    preenchimentoUsuario("Altura", TextInputType.text, "insira sua altura em centímetros.", "Insira uma altura válida", widget.altura), pularLinha(20),
                    preenchimentoUsuario("Peso", TextInputType.text, "Insira seu peso em quilogramas.", "Insira um peso válido", widget.peso), pularLinha(20),
                    preenchimentoUsuario("Senha", TextInputType.text, "Insira uma senha.", "Insira uma senha com pelo menos 8 caracteres", widget.senha), pularLinha(20),
                    pularLinha(80),
                    Container(
                      child: TextButton(
                        child: Text(
                          'Concluir',
                            style: GoogleFonts.getFont(
                              fonte,
                              fontWeight: FontWeight.bold,
                              color: Color(corPrimaria),
                              fontSize: tamanhoLetra,
                          ),
                        ),
                        onPressed: () {
                          final validar = _formKey.currentState!.validate();
                          if (validar) {
                            _formKey.currentState!.save();
                            //Navigator.push(context, MaterialPageRoute(builder: (context) => Menu.withA(widget.email, widget.nome, widget.altura, widget.peso, widget.senha)));
                          }
                        }
                      ),
                    ),
                  ],
                ),
              ],
            ),
        ),
      ),
    );
  }

  preenchimentoUsuario(String texto, TextInputType tipoTeclado, String texto1, String texto2, var teste) {
    return Padding(
    padding: EdgeInsets.only(right: 20, left: 20,),
      //child: Material(
        //borderRadius: BorderRadius.all(Radius.circular(tamanhoRaio),), // raio da borda
        //elevation: tamanhoSombra,
        //shadowColor: Color(corSombra),
        //color: Color(corFundo),
        child: TextFormField(     
          validator: (value) {
            if (value == null || value.isEmpty) {
              return texto1;
            }
            else if (value.length < 3) {
              return texto2;
            }
            return null;
          },
          onSaved: (value) {
            teste = value!;
          },
            keyboardType: tipoTeclado,
            textAlign: TextAlign.center,
            decoration: new InputDecoration(
              hintText: texto, // Texto da caixa de texto "senha"
              hintStyle: GoogleFonts.libreFranklin(
              textStyle: TextStyle( // estilo do texto que vai ser exibido
                color: Color(corPrimaria),
                fontWeight: FontWeight.w400,
                fontSize: tamanhoLetra,
              ),
            ),/*
            focusedBorder: OutlineInputBorder( // borda quando esta selecionada
              borderSide: BorderSide(color: Color(corPrimaria)), // Cor da borda
              borderRadius: BorderRadius.all(Radius.circular(tamanhoRaio),), // Raio da borda
            ),
            enabledBorder: OutlineInputBorder( // borda quando nao esta selecionada
              borderSide: BorderSide(color: Color(corPrimaria)), // Cor da borda
              borderRadius: BorderRadius.all(Radius.circular(tamanhoRaio),), // Raio da borda
            ),*/
          ),
          style: GoogleFonts.getFont(
            fonte,
            fontSize: tamanhoLetra,
            color: Color(corPrimaria),
          ),
        ),
      //),
    );
  }

  /*botaoComRolagem() {
    return DropdownButton<String>(
      items: <String>['A', 'B', 'C', 'D'].map((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: new Text(value),
        );
      }).toList(),
      onChanged: (_) {},
    );
  }*/
}*/