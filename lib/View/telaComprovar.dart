import 'package:flutter/material.dart'; // import padrao do dart
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/Model/metodosTela.dart'; // import dos métodos usados
import 'dart:math'; // gera codigo aleatorio

class Codigo extends StatefulWidget {

  @override
  _CodigoState createState() => _CodigoState();
}

class _CodigoState extends State<Codigo> {
  var rng = new Random();

  //String get email => null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(corFundo),
      body: Container(
        padding: EdgeInsets.only(top: 20, right: 40, left: 40,),
        child: ListView(
          children: [
            caixaDeTexto("Para confirmar o cadastro, foi enviado um código para ---- e que deverá ser inserido na caixa abaixo.", corPrimaria, tamanhoLetra, TextAlign.center), pularLinha(20),
            preenchimentoUsuario("", TextInputType.text), pularLinha(20),
            //numeroAleatorio(rng),
            botaoSemBorda("Enviar o código novamente", 'cadastro', corPrimaria, context), pularLinha(20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                botaoSemBorda("Retornar", 'cadastro', corPrimaria, context),
                botaoSemBorda("Concluir", 'cadastro', corPrimaria, context),
              ],
            ),
          ],
        ),
      ),
    );
  }

  /*numeroAleatorio(var rng) {
    this.rng = rng;
    for (var i = 0; i < 10; i++) {
      print(rng.nextInt(9999));
    }
    return Text(
      ""+rng,
      textAlign: TextAlign.center,
    );
    //String rngString = rng.toString();
    //return rngString;
  }*/

  //@override
  preenchimentoUsuario(String texto, TextInputType tipoTeclado) {
    return Padding(
    padding: EdgeInsets.only(right: 20, left: 20,),
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(tamanhoRaio),), // raio da borda
        elevation: tamanhoSombra,
        shadowColor: Color(corSombra),
        color: Color(corFundo),
        child: TextField(
            keyboardType: tipoTeclado,
            textAlign: TextAlign.center,
            decoration: new InputDecoration(
              hintText: texto, // Texto da caixa de texto "senha"
              hintStyle: GoogleFonts.libreFranklin(
              textStyle: TextStyle( // estilo do texto que vai ser exibido
                color: Color(corPrimaria),
                fontWeight: FontWeight.w400,
                fontSize: tamanhoLetra,
              ),
            ),
            focusedBorder: OutlineInputBorder( // borda quando esta selecionada
              borderSide: BorderSide(color: Color(corPrimaria)), // Cor da borda
              borderRadius: BorderRadius.all(Radius.circular(tamanhoRaio),), // Raio da borda
            ),
            enabledBorder: OutlineInputBorder( // borda quando nao esta selecionada
              borderSide: BorderSide(color: Color(corPrimaria)), // Cor da borda
              borderRadius: BorderRadius.all(Radius.circular(tamanhoRaio),), // Raio da borda
            ),
          ),
          style: GoogleFonts.getFont(
            fonte,
            fontSize: tamanhoLetra,
            color: Color(corPrimaria),
          ),
        ),
      ),
    );
  }
}