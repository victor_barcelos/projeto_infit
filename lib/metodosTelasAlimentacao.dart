import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/Model/metodosTela.dart';

// variaveis para trocar: cor de fundo, cor primaria, cor secundaria, tamanho das sombras, tamanho da letra e tamanho do raio das caixas
int corPrimaria = 0xFF32CBFF;
int corSecundaria = 0xFF00A5E0;
int corFundo = 0xFF80CBC4;
int corSuporteFundo = 0xFFFFFFFF;
int corSombra = 0xFF333333;
int corDoTexto = 0xFFFFFFFF;
int corSuporteTexto = 0xFFFFFFFF;
int corSuporteReceita = 0xFF8BC34A;
int corSuporteFundoVerde = 0xFF1B5E20;

double tamanhoRaio = 40;
double tamanhoSombra = 5;
double tamanhoLetra = 15;
double tamanhoLetraTitulos = 25;
double tamanhoLetraSubtitulos = 15;

//String fonte = 'Gothic A1';
String fonte = 'Dosis';
String fonteReceitas = 'Dosis';

pularLinha(double pular) {
  return SizedBox(height: pular,);
}

botaoSemBorda(String texto, String pagina, int corTexto, BuildContext context) {
  return Center(
    // center
    child: TextButton(
      child: Text(
        texto, // texto que sera exibido
        style: GoogleFonts.getFont(
          fonte,
          fontWeight: FontWeight.bold,
          color: Color(corTexto),
          fontSize: tamanhoLetra,
        ),
      ),
      onPressed: () =>
          Navigator.pushNamed(context, pagina), // OnPressed genérico
    ),
  );
}

// método de criar um botao sem borda com imagem
botaoSemBordaComImagem(String pagina, BuildContext context, String caminhoDaImagem) {
  return Center(
    child: MaterialButton(
      color: Colors.white,
      child: CircleAvatar(
        radius: 70.0,
        backgroundColor: Colors.white,
        child: CircleAvatar(
          radius: 65,
          backgroundImage: AssetImage(caminhoDaImagem),
        ),
      ),
      shape: CircleBorder(),
      onPressed: () =>
          Navigator.pushNamed(context, pagina), // OnPressed genérico
    ),
  );
}

// método de criar um botao com borda sem ação
botaoComBorda(String texto, String pagina, int corBotao, int corTexto, BuildContext context) {
  return Center(
    child: Material(
      // material para adicionar sombras e relevo
      elevation: tamanhoSombra, // relevo/ tamanho da sombra
      color: Color(corBotao), // cor do botao
      shadowColor: Color(corSombra), // cor da sombra
      borderRadius: BorderRadius.all(
        Radius.circular(tamanhoRaio),
      ), // raio da borda do botao

      child: TextButton(
        child: Text(
          texto, // texto que sera exibido
          style: GoogleFonts.getFont(
            fonte,
            fontWeight: FontWeight.bold,
            color: Color(corTexto),
            fontSize: tamanhoLetra,
          ),
        ),
        onPressed: () =>
            Navigator.pushNamed(context, pagina), // OnPressed genérico
      ),
    ),
  );
}

// método de criar uma caixa de texto
caixaDeTexto(String texto, int corTexto, double tamanhoFonte, TextAlign ajustar) {
  return Container(
    // container
    child: Text(
      texto, // texto que sera exibido
      textAlign:
          ajustar, // como ele será ajustado. ex: centro, direita, esquerda, topo direita, etc...
      style: GoogleFonts.getFont(
        fonte,
        fontWeight: FontWeight.bold,
        color: Color(corTexto),
        fontSize: tamanhoFonte,
      ),
    ),
  );
}

botaoCorEstado(String pagina, String texto, BuildContext context, String imagem) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Material(
        borderRadius: BorderRadius.all(
          Radius.circular(50),
        ), // raio da borda
        elevation: tamanhoSombra,
        shadowColor: Color(corSombra),
        color: Colors.transparent,
        child: GestureDetector(
          child: CircleAvatar(
            radius: 60,
            backgroundColor: Color(corSuporteFundo),
            child: CircleAvatar(
              radius: 55,
              backgroundImage: AssetImage(imagem),
            ),
          ),
          onTap: () => Navigator.pushNamed(context, pagina),
        ),
      ),
      pularLinha(10),
      caixaDeTexto(texto, corSuporteTexto, tamanhoLetra, TextAlign.center),
    ],
  );
}

layoutReceitas(context, nomeDaReceita, textoIngredientes, textoModoDePreparo, paginaDeRetorno,) {
  return MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      backgroundColor: Color(corFundo),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              pularLinha(10),
              Container(
                padding: EdgeInsets.all(10),
                child: Card(
                  elevation: tamanhoSombra, // relevo/ tamanho da sombra
                  // cor do botao
                  shadowColor: Colors.black,
                  color: Color(corSuporteFundo),
                  child: Padding(
                    padding: EdgeInsets.only(
                      top: 20,
                      right: 20,
                      left: 10,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          children: [
                            Text(
                              nomeDaReceita,
                              style: GoogleFonts.getFont(fonte,
                                color: Color(0xFFE65100/*0xFF000000*/),
                                fontSize: tamanhoLetraTitulos,
                                fontWeight: FontWeight.bold),
                            ), pularLinha(10),
                            Center(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SizedBox(
                                    height: 20.0,
                                    width: 60.0,
                                    child: Divider(
                                      color: Colors.black,
                                    ),
                                  ),
                                  Text(
                                    'Ingredientes',
                                    style: GoogleFonts.getFont(
                                      fonte,
                                      color: Color(0xFFE65100),
                                      fontSize: tamanhoLetraSubtitulos,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                    width: 60.0,
                                    child: Divider(
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(10),
                            ),
                            Text(
                              textoIngredientes,
                              textAlign: TextAlign.justify,
                              style: GoogleFonts.getFont(fonte,
                                  fontSize: tamanhoLetraSubtitulos,
                                  color: Color(corSombra)),
                            ),
                            Container(
                              padding: EdgeInsets.all(10),
                            ),
                            Center(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  SizedBox(
                                    height: 20.0,
                                    width: 60.0,
                                    child: Divider(
                                      color: Colors.black,
                                    ),
                                  ),
                                  Text(
                                    '    Modo de preparo',
                                    style: GoogleFonts.getFont(fonte,
                                        color: Color(0xFFE65100),
                                        fontSize: tamanhoLetraSubtitulos,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(6),
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                    width: 60.0,
                                    child: Divider(
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(10),
                            ),
                            Text(
                              textoModoDePreparo,
                              style: GoogleFonts.getFont(fonte,
                                  fontSize: tamanhoLetraSubtitulos,
                                  color: Color(corSombra)),
                            ),
                            Container(
                              padding: EdgeInsets.all(10),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      botaoComBorda('Retornar', paginaDeRetorno,corSuporteFundo, corSombra, context),
                      Container(padding: EdgeInsets.all(10),),
                      botaoComBorda('Salvar Receita', '', corSuporteFundo,corSombra, context),
                      Container(margin: EdgeInsets.only(top: 20, bottom: 50.0),),
                    ],
                  ),
                  pularLinha(30),
                ]
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

iconeMenu(context, corFundoIcone, corIcone, Alignment ajustar) {
  return Container(
    alignment: ajustar,
    color: Color(corFundo),
    child: CircleAvatar(
      radius: 20,
      backgroundColor: Color(corFundoIcone),
      child: GestureDetector(
        child: Icon(
          Icons.settings,
          color: Color(corIcone),
        ),
        onTap: () => Navigator.pushNamed(context, 'MenuAlimentos')
      ),
    ),
  );
}

