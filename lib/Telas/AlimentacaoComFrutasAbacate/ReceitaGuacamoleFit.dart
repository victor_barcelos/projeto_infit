import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaGuacamoleFit());

class ReceitaGuacamoleFit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Guacamole Fit ',
        '↪ 1 abacate médio maduro;\n↪ 2 tomates picados sem pele e sem sementes;\n↪ 1 cebola média picada;\n↪ 1 dente de alho amassado;\n↪ 2 colheres de sopa de azeite;\n↪ Pimenta do reino a gosto;\n↪ Suco de 1 limão;\n↪ Sal a gosto;\n↪ Cheiro-verde fresco a gosto.',
        '↪ Corte o abacate ao meio, retire o caroço e remova a polpa. Corte em pedaços, amasse com a ponta de um garfo e leve para gelar;\n\n↪ Enquanto isso, coloque o tomate picado, a cebola e o alho para refogar em fogo baixo com um pouco de azeite. Adicione pimenta e uma colher de sopa de água. Tampe e deixe cozinhar por 2 minutos;\n\n↪  Espere esfriar, junte o abacate e misture até formar uma pasta. Tempere com sal, limão e cheiro verde;\n\n↪ Leve para gelar e sirva com tortilhas de milho.',
        'TelaComFrutasAbacate');
  }
}
