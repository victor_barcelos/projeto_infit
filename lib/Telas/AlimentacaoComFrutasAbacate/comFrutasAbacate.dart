import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(TelaComFrutasAbacate());

class TelaComFrutasAbacate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Color(corFundo),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                top: 20,
                right: 40,
                left: 40,
              ),
              child: SafeArea(
                child: Column(children: [
                  Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                      Center(
                        child: Text(
                          'Com frutas: abacate',
                          style: GoogleFonts.getFont(fonte,
                              color: Color(corSuporteTexto),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaGuacamoleFit',
                              'Guacamole Fit',
                              context,
                              'assets/imagens/comFrutas/comFrutasAbacate/guacamole-fit.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaSaladaComAbacate',
                              'Salada de abacate',
                              context,
                              'assets/imagens/comFrutas/comFrutasAbacate/salada-com-abacate.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaMousseDeAbacate',
                              'Mousse de abacate',
                              context,
                              'assets/imagens/comFrutas/comFrutasAbacate/mousse-de-abacate.jpeg'),
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaAbacateRecheado',
                              'Abacate rechado',
                              context,
                              'assets/imagens/comFrutas/comFrutasAbacate/abacate-recheado.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaBrigadeiroDeAbacate',
                              'Brigadeiro de abacate',
                              context,
                              'assets/imagens/comFrutas/comFrutasAbacate/brigadeiro-de-abacate.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaSorveteDeAbacate',
                              'Sorvete de abacate',
                              context,
                              'assets/imagens/comFrutas/comFrutasAbacate/sorvete-de-abacate.png'),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 20,
                      right: 20,
                      left: 40,
                    ),
                  ),
                  botaoSemBorda(
                      'Retornar', 'comFrutas', corSuporteTexto, context)
                ]),
              ),
            ),
          ),
        ));
  }
}
