import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaSaladaComAbacate());

class ReceitaSaladaComAbacate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Salada com abacate',
        '↪ 1 abacate médio maduro e cortado em cubos;\n↪ 500 gramas de tomate cereja cortados ao meio;\n↪ 1 pepino cortado em cubos;\n↪ 1 cebola roxa média cortada em cubos;\n↪ 1/2 manga cortada em fatias;\n↪ 1 colher se sopa de gergelim.',
        '↪ Higienize os ingredientes e corte-os como orientado;\n\n↪ Depois basta separar uma saladeira e misturar todos os ingredientes;\n\n↪  Tempere como preferir com um pouco de sal, azeite e limão. Leve para gelar até a hora de servir.',
        'TelaComFrutasAbacate');
  }
}
