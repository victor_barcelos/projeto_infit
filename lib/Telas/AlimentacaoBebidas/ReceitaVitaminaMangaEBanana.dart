import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaVitaminaMangaEBanana());

class ReceitaVitaminaMangaEBanana extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Manga e banana',
        '↪ 2 copos de leite desnatado;\n↪ 1 copo de suco de laranja natural;\n↪ 1 banana e 1 manga picadas;\n↪ 1 colher de semente de girassol; \n↪ 2 colheres de whey protein.',
        '↪ Bata todos os ingredientes no liquidificador e sirva gelado.',
        'Bebidas');
  }
}
