import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaVitaminaBananaMorangoAveia());

class ReceitaVitaminaBananaMorangoAveia extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Banana com morango',
        '↪ 5 morangos;\n↪ 1 copo de leite;\n↪ 1 banana madura;\n↪ 1 copo de leite;\n↪ Açúcar e aveia a gosto.',
        '↪ Bata todos os ingredientes no liquidificador e sirva gelado.',
        'Bebidas');
  }
}
