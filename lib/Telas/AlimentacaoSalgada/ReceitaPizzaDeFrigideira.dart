import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaPizzaDeFrigideira());

class ReceitaPizzaDeFrigideira extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Pizza de frigideira',
        '↪ 1 ovo;\n↪ 1 xícara de farinha/farelo de aveia;\n↪ 1/2 colher de chá de sal\n↪ 1/2 colher de chá de fermento;\n↪ Azeite; \n↪ Recheio: queijo, frango, tomate em rodelas, molho de tomate, carne do sol desfiada, azeitona e o que mais você preferir.',
        '↪ Em um recipiente, misture todos os ingredientes até obter uma mistura homogênea;\n\n↪ Aqueça a frigideira, unte com azeite ou manteiga;\n\n↪ Com o fogo baixo, despeje a massa e a espalhe com auxílio de um garfo. Tampe.\n\n↪ Quando o lado de baixo estiver dourado, vire a massa;\n\n↪ Adicione molho de tomate, queijo, orégano, tomate em rodelas, frango, azeitona e outros recheios de sua preferência.\n\n↪ Adicione um pouco de água na panela para que não queime enquanto o queijo derrete. Agora, é só servir!',
        'salgadas');
  }
}
