import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaMacarraoAMilanesa());

class ReceitaMacarraoAMilanesa extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Macarrão à milanesa ',
        '↪ 1/2kg de frango desfiado;\n↪ 1/2 limão;\n↪ 1 pote de creme de ricota light;\n↪ 1/2 litro de leite desnatado;\n↪ 1 cebola picada;\n↪ 4 dentes de alho picado;\n↪ Cheiro verde e sal a gosto;\n↪ Azeite;\n↪ Queijo ralado;\n↪ Macarrão integral.                          ',
        '↪ Na palena de pressão, coloque o frango, o suco do limão, alho, a cebola, cheiro verde e demais temperos de sua preferência. Cubra tudo com água e deixe cozinhar até o ponto de desfiar (aproximadamente meia hora);\n\n↪ Enquanto o frango cozinha, vamos para o molho: em uma frigideira antiaderente, coloque a cebola picada e o alho, e refolgue até dourar;\n\n↪ Depois de refogados, adicione o leite (o ideal é que seja desnatado e 0 gordura). Adicione cheiro verde, sal e demais temperos de sua preferência. Adicione o creme de ricota e misture.\n\n↪ Desfie o frango e o misture ao molho branco.\n\n↪ Prepare o macarrão da forma como preferir, e pronto. AGora é só servir!',
        'salgadas');
  }
}
