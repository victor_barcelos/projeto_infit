import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaHamburguer());

class ReceitaHamburguer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Hambúrguer Fit',
        '↪ Queijo cheddar light;\n↪ 50g de linguiça colonial;\n↪ 450g de carne patinho\n↪ Pimenta do reino, pápicra picante, manjericão, sal e demais temperos a gosto;\n↪ 1/3 de cebola em rodelas;\n↪ Alface e tomate a gosto;\n↪ Pão hambúrguer ou francês.\n↪ Molhos: ketchup zero, maionese light, mostarda amarela, molho de alho e demais de sua preferência.',
        '↪ "Desmonte" a linguiça, deixe-a bem moída para adicioná-la na carne. Misture as duas e adicione os temperos. Não coloque o sal agora;\n\n↪ Divida as porções de carne para cada hambúrguer e molde cada uma delas;\n\n↪ Em uma panela anti-aderente, coloque os hambúrgueres e refolgue a cebola junto com os hambúrgueres;\n\n↪ Quando a carne estiver quase pronta, coloque o queijo por cima;\n\n↪ Monte os hambúrgueres: adicione o molho, a cebola, alface e tomate. Agora, é só servir!',
        'salgadas');
  }
}
