import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaFricasse());

class ReceitaFricasse extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Fricassê de frango',
        '↪ 4 colheres de sopa requeijão sem lactose;\n↪ 1 cebola picada;\n↪ 3 dentes de alho\n↪ 1 lata de milho verde;\n↪ 500g de frango desfiado; \n↪ 200 g de mussarela fatiada light;\n↪ Sal a gosto; \n↪ Óleo de coco.',
        '↪ Refolgue a cebola e o alho no óleo de coco. Acrescente o frango desfiado, o milho e temperos de sua preferência;\n\n↪ Acrescente o creme de leite até que fique cremoso;\n\n↪ Num refratário, coloque o frango, o requeijão (por cima do frango) e o queijo;\n\n↪ Coloque no forno pré-aquecido a 180° até que o queijo derreta.\n\n↪ Agora, é só servir!',
        'salgadas');
  }
}
