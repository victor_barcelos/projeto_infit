import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaBoloDeAbacaxi());

class ReceitaBoloDeAbacaxi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Bolo de abacaxi ',
        '↪ 2 rodelas de abacaxi descascado;\n↪ ½ xícara (chá) de água;\n↪ 2 colheres (sopa) de pasta de amendoim com chocolate branco light;\n↪ 1 xícara (chá) de óleo de amêndoas;\n↪ 2 xícaras (chá) de farinha de trigo integral; \n↪ 1 xícara (chá) de aveia em flocos; \n↪ 1 colher (sopa) de linhaça; \n↪ 3 ovos;\n↪ 1 colher (sopa) de fermento;\n↪ Canela a gosto.',
        '↪ Primeiro, faça um suco de abacaxi (com uma rodela da fruta com a água). Na sequência, bata do liquidificador a bebida junto com o óleo, a pasta de amendoim e os ovos;\n\n↪ Então, em um recipiente diferente, coloque o restante dos ingredientes secos (com exceção da outra rodela de abacaxi) e misture;\n\n↪ Junte o abacaxi e refogue por mais 2 minutos. Feito isso, vá despejando aos poucos o conteúdo líquido nessa mistura até que ela fique bem úmida;\n\n↪ Para finalizar, pique a segunda rodela de abacaxi e adicione à massa do bolo.Agora, é só colocar para assar.\n\n↪ Unte uma forma com manteiga e transfira o bolo para a nova vasilha.Leve ao forno a uma temperatura média de 200°C por cerca de 45 minutos e pronto!',
        'TelaComFrutasAbacaxi');
  }
}
