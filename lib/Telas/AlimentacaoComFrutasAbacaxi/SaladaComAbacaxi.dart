import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaSaladaComAbacaxi());

class ReceitaSaladaComAbacaxi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Salada com abacaxi ',
        '↪ Rúcula;\n↪ Radicchio;\n↪ Tomate italiano;\n↪ Fatias de abacaxi;\n↪ Palitos de queijo;\n↪ Vinagre balsâmico;\n↪ Sal e azeite de oliva a gosto.',
        '↪ Lave bem as folhas e disponha no prato. Pique o rabanete em tirinhas e o tomate também;\n\n↪ Para grelhar o abacaxi, coloque um fio de azeite numa frigideira e grelhe dos dois lados sem tampar;\n\n↪ Salpique uma pitada de sal e um fio de azeite nas folhas e outros legumes e coloque o um pouco de vinagre balsâmico em cima do tomate.\n\n↪ Sirva com alguns palitos de queijo grelhados.',
        'TelaComFrutasAbacaxi');
  }
}
