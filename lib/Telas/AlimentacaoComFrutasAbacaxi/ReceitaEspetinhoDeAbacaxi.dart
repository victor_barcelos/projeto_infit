import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaEspetinhoDeAbacaxi());

class ReceitaEspetinhoDeAbacaxi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Espetinho de abacaxi ',
        '↪ 1 kg de patinho em cubos médios;\n↪ 1 abacaxi em cubos médios;\n↪ Cebola Roxa;\n↪ Sal e pimenta do reino a gosto;\n↪ Açúcar;\n↪ Azeite;\n↪ Gergelim; ;\n↪ Cebolinha;\n↪ 1/4 de xícara de vinagre.',
        '↪ Em um recipiente pequeno, misture o vinagre e a água. Junte o açúcar e uma pitada de sal. Misture bem.;\n\n↪ Coloque as cebolas neste liquido, tampe com papel filme. Faça alguns furos no filme e leve ao micro ondas por 2 minutos. Retire o plástico e reserve;\n\n↪ Tempere o lombo com sal, pimenta e demais temperos de sua preferência. Reserve.\n\n↪ Em uma frigideira coloque duas colheres de sopa de azeite e uma pitada de açúcar. Espere aquecer;\n\n↪ Coloque os cubos de lombo e deixe dourar de todos os lados. Reserve;\n\n↪ Na mesma frigideira, coloque os cubos de abacaxi e deixe dourar de todos os lados. Reserve;\n\n↪ Em espetinhos pequenos coloque um cubo de carne, uma pétala de cebola e um cubo de abacaxi;\n\n↪ Polvilhe cebolinha e gergelim na hora de servir.',
        'TelaComFrutasAbacaxi');
  }
}
