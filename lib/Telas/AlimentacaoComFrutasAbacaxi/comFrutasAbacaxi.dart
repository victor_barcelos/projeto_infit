import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(TelaComFrutasAbacaxi());

class TelaComFrutasAbacaxi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Color(corFundo),
          body: SingleChildScrollView(
            child: SafeArea(
              child: Column(children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        top: 40,
                        right: 20,
                        left: 40,
                      ),
                    ),
                    Text(
                      ' Com frutas: abacaxi ',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.getFont(fonte,
                          color: Color(corSuporteTexto),
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        top: 20,
                        right: 20,
                        left: 40,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        botaoCorEstado(
                            'ReceitaSaladaComAbacaxi',
                            'Salada com abacaxi',
                            context,
                            'assets/imagens/comFrutas/comFrutasAbacaxi/salada-com-abacaxi.jpg'),
                        Container(
                          padding: EdgeInsets.only(
                            top: 20,
                            right: 20,
                            left: 40,
                          ),
                        ),
                        botaoCorEstado(
                            'ReceitaSorveteDeAbacaxi',
                            'Sorvete de abacaxi',
                            context,
                            'assets/imagens/comFrutas/comFrutasAbacaxi/sorvete-de-abacaxi.jpeg'),
                        Container(
                          padding: EdgeInsets.only(
                            top: 20,
                            right: 20,
                            left: 40,
                          ),
                        ),
                        botaoCorEstado(
                            'ReceitaPeixeComAbacaxi',
                            'Peixe com abacaxi',
                            context,
                            'assets/imagens/comFrutas/comFrutasAbacaxi/peixe_com_abacaxi.jpg'),
                      ],
                    ),
                    Column(
                      children: [
                        botaoCorEstado(
                            'ReceitaEspetinhoDeAbacaxi',
                            'Espetinho com abacaxi',
                            context,
                            'assets/imagens/comFrutas/comFrutasAbacaxi/espetinho_abacaxi.jpg'),
                        Container(
                          padding: EdgeInsets.only(
                            top: 20,
                            right: 20,
                            left: 40,
                          ),
                        ),
                        botaoCorEstado(
                            'ReceitaMousseDeAbacaxi',
                            'Mousse de abacaxi',
                            context,
                            'assets/imagens/comFrutas/comFrutasAbacaxi/mousse-de-abacaxi.jpg'),
                        Container(
                          padding: EdgeInsets.only(
                            top: 20,
                            right: 20,
                            left: 40,
                          ),
                        ),
                        botaoCorEstado(
                            'ReceitaBoloDeAbacaxi',
                            'Bolo de abacaxi',
                            context,
                            'assets/imagens/comFrutas/comFrutasAbacaxi/bolo_de_abacaxi.jpg'),
                      ],
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: 20,
                    right: 20,
                    left: 40,
                  ),
                ),
                botaoSemBorda('Retornar', 'comFrutas', corSuporteTexto, context)
              ]),
            ),
          ),
        ));
  }
}
