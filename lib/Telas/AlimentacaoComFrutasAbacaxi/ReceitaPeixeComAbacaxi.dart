import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaPeixeComAbacaxi());

class ReceitaPeixeComAbacaxi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Peixe com abacaxi ',
        '↪ 1/2 kg de salmão cortado em filés;\n↪ 1/2 colher (chá) de sal;\n↪ 3 colheres (sopa) de azeite de oliva;\n↪ 2 dentes de alho picados;\n↪ 150g de cogumelos fatiados; \n↪ 6 fatias finas de abacaxi; \n↪ 2 colheres de mel; \n↪ 1/2 xícra (chá) de maionese light sabor cebolas tostadas e orégano;\n↪ 1 colher (chá) de cheiro-verde picado.',
        '↪ Tempere os filés de salmão com o sal e reserve. Em uma frigideira grande ou grelha aqueça 2 colheres de sopa do azeite de oliva e frite os filés até dourar dos dois lados. Retire do fogo e coloque em uma travessa, mantendo aquecidos;\n\n↪ Na mesma frigideira aqueça o restante do azeite e refogue o alho e os cogumelos por 2 minutos;\n\n↪ Junte o abacaxi e refogue por mais 2 minutos. Acrescente o mel e a maionese sabor cebolas tostadas e orégano e cozinhe por mais 2 minutos ou até obter um molho cremoso; \n\n↪ Despeje sobre os filés de peixe reservados, polvilhe o cheiro-verde e sirva em seguida.',
        'TelaComFrutasAbacaxi');
  }
}
