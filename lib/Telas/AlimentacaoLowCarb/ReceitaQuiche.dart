import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaQuiche());

class ReceitaQuiche extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Quiche de espinafre',
        '↪ 1 xícara de chá de farinha de amêndoas; \n↪ 1/4 xícara de chá de azeite de oliva; \n↪ 1 pitada de sal; \n↪ 1 maço de espinafre lavado; \n↪ 4 unidades de ovo; \n↪ 1/4 xícara de chá de creme de leite fresco (ou leite vegetal); \n↪ 1 colher de chá de fermento; \n↪ 1 xícara de chá de queijo parmesão ralado.',
        '↪ Em um bowl, misture todos os ingredientes da massa (farinha de amêndoa, azeite e sal) até formar uma massa. Forre o fundo e as laterais de uma fôrma média de aro removível;\n\n↪ Leve ao forno preaquecido a 180 °C por 15 minutos. Reserve.\n\n↪ Refogue o espinafre com 1 fio de azeite por 2 minutos. Junte aos ovos, ao creme de leite, ao fermento e ao parmesão, misture bem e coloque sobre a massa. \n\n↪ Leve ao forno por mais 20 minutos ou até assar completamente (ou faça o teste do palito).',
        'TelaLowCarb');
  }
}
