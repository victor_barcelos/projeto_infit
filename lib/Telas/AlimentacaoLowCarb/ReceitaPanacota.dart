import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaPanacota());

class ReceitaPanacota extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Panacota com morango',
        '↪ 200ml de leite de coco;\n↪ 1 colher de cá de ágar-ágar;\n↪ 1/2 xícara de adoçante (xylitol);\n↪ 400ml de iogurte grego; \n↪ 1/2 xícara de chá de morango orgânico; \n↪ Suco de meio limão; \n↪ 1 colher (sobremesa) de canela em pó; \n↪ 1/2 colher (sobremesa) de gengibre em pó; 1/2 colher (sobremesa) de melaço de cana-de-açúcar ou mel; \n↪ Água suficiente para ajudar a bater.',
        '↪ Panacota: 1. Em uma panela, coloque o leite de coco, o ágar-ágar e o xylitol, e misture bem. Leve ao fogo, mexendo com um batedor de arame (fouet) sem parar até ferver; \n\n↪ 2. Após ferver, siga mexendo sem parar por mais 3 minutos, desligue o fogo e espere amornar. Incorpore o iogurte (que precisa estar em temperatura ambiente) misturando sempre.\n\n↪ 3. Coloque o creme em potinhos de vidro e leve à geladeira por 30 minutos;\n\n↪ Calda: 1. Bata todos os ingredientes no liquidificador ou processador, até obter uma consistência de calda.\n\n↪ 2. Coloque a mistura por cima da panacota e leva à geladeira por 2 horas. Se quiser, coloque morangos por cima para decorar.',
        'TelaLowCarb');
  }
}
