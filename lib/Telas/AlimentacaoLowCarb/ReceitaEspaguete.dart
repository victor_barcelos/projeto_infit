import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaEspaguete());

class ReceitaEspaguete extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Espaguete de abobrinha',
        '↪ 1 abobrinha grande cortada em formato de espaguete;\n↪ 1 bandeja de espaguete de pupunha;\n↪ Pesto de agrião; \n↪1 colher de sopa de óleo de girassol;\n↪ 1 cebola cortada em tirinhas;\n↪ 1 punhado de brotos de beterraba, brotos de nabo e amêndoas.',
        '↪ Escalde rapidamente a pupunha. Corte a abobrinha no formato indicado, e a cebola também; \n\n↪ Refogue a abobrinha, a pupunha e a cebola no óleo por 2; \n\n↪ Adicione o pesto a gosto e misture bem; \n\n↪ Disponha o espaguete no prato, adicione azeite, os brotinhos, a beterraba, cenoura, amêndoas. Sirva em seguida.',
        'TelaLowCarb');
  }
}
