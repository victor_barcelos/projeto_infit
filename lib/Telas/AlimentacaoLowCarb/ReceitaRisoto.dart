import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaRisoto());

class ReceitaRisoto extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Risoto de couve-flor',
        '↪ 1 couve-flor média cozida até o ponto al dente;\n↪ 2 colheres (sopa) de creme de ricota light;\n↪ 1 colher (sopa) de azeite de oliva extravirgem;\n↪ Sal a gosto.',
        '↪ Bata a couve-flor no processador até ficar em pedaços do tamanho de grãos de arroz; \n\n↪ Misture todos os ingredientes numa tigela e leve ao forno, panela ou micro-ondas para aquecer. Se quiser, tempere com pimenta, cheiro-verde, azeite ou o que mais preferir. Sirva quente.',
        'TelaLowCarb');
  }
}
