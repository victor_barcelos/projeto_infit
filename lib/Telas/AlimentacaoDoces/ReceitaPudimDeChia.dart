import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaPudimDeChia());

class ReceitaPudimDeChia extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Pudim de chia',
        '↪ 150ml de purê de framboesa;\n↪ 150ml de água;\n↪ 7g de gelatina em pó sem sabor;\n↪ 400ml de leite de coco;\n↪ 6 colheres de chia;\n↪ 3 colheres de sopa de agave.',
        '↪ Gelatina: 1. Em um processador pequeno, processe as framboesas até que vire um purê;\n\n↪ 2. Esquente a água e jogue por cima da gelatina;\n\n↪ 3. Misture até que a gelatina se dissolva;\n\n↪ 4. Misture a gelatina no purê de framboesa, e monte no fundo de um potinho;\n\n↪ 5. Leve para a geladeira e espere endurecer (aproximadamente 2 horas);\n\n↪ Pudim: 1. Em um recipiente com tampa, misture o leite de coco, a chia e o agave;\n\n↪ 2. Feche o recipiente e chacoalhe bem;\n\n↪ 3. Retire da geladeira o potinho com a gelatina de framboesa e complete com o pudim de chia;\n\n↪ 4. Retorne para a geladeira por 4 horas;\n\n↪ Retire da geladeira e sirva.',
        'TelaDoces');
  }
}
