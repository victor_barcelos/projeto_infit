import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaPudimDeLeite());

class ReceitaPudimDeLeite extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Pudim de leite',
        '↪ 6 ovos;\n↪ 2 xícaras (chá) leite em pó desnatado;\n↪ 2 xícaras (chá) leite desnatado;\n↪ 1 xícara (chá) iogurte natural desnatado;\n↪ 3 colheres (sopa) adoçante culinário;\n↪ 3 colheres (sopa) açúcar demerara;\n↪ 1 colher (sopa) água.',
        '↪ Bater os ovos, o leite em pó e o leite normal, o iogurte e o adoçante. Reserve;\n\n↪ Coloque o açúcar e a água na forma e coloque sobre o fogo médio até formar uma calda de caramelo;\n\n↪ Despeje delicadamente o líquido do pudim sobre a calda;\n\n↪ Leve ao forno em banho maria por 40 minutos.',
        'TelaDoces');
  }
}
