import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaTamaraECoco());

class ReceitaTamaraECoco extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Tâmara e coco',
        '↪ 1/2 xícara de tâmaras sem caroço;\n↪ 1/2 xícara de coco seco ralado sem açúcar; \n↪ 80 gramas de chocolate meio amargo 50 ou 70% cacau;\n↪ Coco ralado para cobrir os docinhos.',
        '↪ Coloque as tâmaras e uma tigela, cubra com água fervente, tampe e deixe descansar por 15 minutos;\n\n↪ Retire as tâmaras da água com a ajuda de um garfo (não jogue a água fora) e coloque-as no processador com o coco ralado;\n\n↪ Processe os dois ingredientes com algumas colheradas da água do molho, se preferir pode colocar leite de coco, adicione conforme a necessidade;\n\n↪ A massa deve ficar levemente grudenta, mas de forma que você consiga moldar bolinha;\n\n↪ Deixe na geladeira por 30 minutos antes de enrolar;\n\n↪ Faça bolinhas, se precisar unte as mãos com água para facilitar, depois reserve na geladeira por 20 minutos;\n\n↪ Derreta o chocolate em banho-maria ou microondas, não precisa temperar;\n\n↪ Passe os docinhos no chocolate com a ajuda de um garfo, não precisa ficar perfeito e lisinho como um bombom, é só para fazer uma casquinha fina de chocolate; \n\n↪ Depois passe no coco ralado e boleie delicadamente com a mãos, para dar acabamento e ficarem mais redondas;\n\n↪ Se preferir, coloque em forminhas de brigadeiro e mantenha os docinhos na geladeira.',
        'TelaDoces');
  }
}
