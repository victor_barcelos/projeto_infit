import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(TelaComFrutasKiwi());

class TelaComFrutasKiwi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Color(corFundo),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                top: 20,
                right: 40,
                left: 40,
              ),
              child: SafeArea(
                child: Column(children: [
                  Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                      Center(
                        child: Text(
                          'Com frutas: kiwi',
                          style: GoogleFonts.getFont(fonte,
                              color: Color(corSuporteTexto),
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          top: 20,
                          right: 20,
                          left: 40,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaLimonadaDeKiwi',
                              'Limonada de kiwi',
                              context,
                              'assets/imagens/comFrutas/comFrutasKiwi/limonada-de-kiwi.jpeg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaPanquecaComKiwi',
                              'Panqueca com kiwi',
                              context,
                              'assets/imagens/comFrutas/comFrutasKiwi/panqueca-com-kiwi.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaGeleiaDeKiwi',
                              'Geleia de kiwi',
                              context,
                              'assets/imagens/comFrutas/comFrutasKiwi/geleia-de-kiwi.jpg'),
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaSucoRefrescanteKiwi',
                              'Suco refrescante',
                              context,
                              'assets/imagens/comFrutas/comFrutasKiwi/suco-kiwi.jpeg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaPicoleDeKiwi',
                              'Picolé de kiwi',
                              context,
                              'assets/imagens/comFrutas/comFrutasKiwi/picole-de-kiwi.jpg'),
                          Container(
                            padding: EdgeInsets.only(
                              top: 20,
                              right: 20,
                              left: 40,
                            ),
                          ),
                          botaoCorEstado(
                              'ReceitaCaipirinhaDeKiwi',
                              'Caipirinha de kiwi',
                              context,
                              'assets/imagens/comFrutas/comFrutasKiwi/caipirinha-de-kiwi.jpg'),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 20,
                      right: 20,
                      left: 40,
                    ),
                  ),
                  botaoSemBorda(
                      'Retornar', 'comFrutas', corSuporteTexto, context)
                ]),
              ),
            ),
          ),
        ));
  }
}
