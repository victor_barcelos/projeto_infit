import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaPicoleDeKiwi());

class ReceitaPicoleDeKiwi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Picolé de kiwi',
        '↪ 3 kiwis descascados;\n↪ 1 copo de 200ml de suco de limão;\n↪ 2 colheres de sopa de mel ou açúcar; \n↪ Açúcar a gosto.',
        '↪ Bata todos os ingredientes no liquidificador. Coloque alguns pedaços em rodelas para enfeitar;\n\n↪ Depois despeje nas forminhas de picolé e leve ao freezer até estar no ponto de picolé.',
        'TelaComFrutasKiwi');
  }
}
