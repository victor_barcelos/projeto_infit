import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaSucoRefrescanteKiwi());

class ReceitaSucoRefrescanteKiwi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Suco refrescante',
        '↪ 4 kiwi picados;\n↪ 1 porção de hortelã, lavado e picado;\n↪ Água gelada; \n↪ Açúcar a gosto.',
        '↪ Coloque no liquidificador o kiwi e hortelã, a água e o açúcar. Se preferir, coloque gelo também;\n\n↪ Coe e sirva.',
        'TelaComFrutasKiwi');
  }
}
