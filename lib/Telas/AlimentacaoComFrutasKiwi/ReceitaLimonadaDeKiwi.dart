import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaLimonadaDeKiwi());

class ReceitaLimonadaDeKiwi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Limonada de kiwi',
        '↪ 1 xícara (chá) de água;\n↪ 1/2 xícara (chá) de suco de limão;\n↪ 1 kiwi picado;\n↪ 3 cubos de gelo;\n↪ Adoçante a gosto.',
        '↪ Em um liquidificador adicione a água, suco de limão, kiwi picado e o gelo;\n\n↪ Bata bem até o gelo ficar bem triturado;\n\n↪ Adoce como desejar e sirva logo em seguida. ',
        'TelaComFrutasKiwi');
  }
}
