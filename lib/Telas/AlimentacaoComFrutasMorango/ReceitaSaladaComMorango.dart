import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaSaladaComMorango());

class ReceitaSaladaComMorango extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Salada com morango',
        '↪ 1 maço de rúcula;\n↪ 100g de nozes moídas;\n↪ 1 caixa de morangos;\n↪ 2 colheres de sopa de mostarda;\n↪ 1 colher de sopa de vinagre;\n↪ 1/2 xícara de chá de azeite; \n↪ 1 xícara de iogurte natural sem açúcar;\n↪ 1 pitada de açúcar;\n↪ Suco de 1 limão; \n↪ 1/2 colher de chá de sal; \n↪ Pimenta do reino a gosto.',
        '↪ Lave bem as folhas de rúcula separadas do talo;\n\n↪ Corte ao meio os morangos lavados;\n\n↪ Misture o iogurte, o vinagre, a mostarda, o suco do limão, o açúcar, o sal e a pimenta em uma saladeira;\n\n↪ Acrescente os morangos, a rúcula e as nozes. ',
        'TelaComFrutasMorango');
  }
}
