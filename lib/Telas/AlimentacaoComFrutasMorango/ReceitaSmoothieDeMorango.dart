import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaSmoothieDeMorango());

class ReceitaSmoothieDeMorango extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Smoothie de morango',
        '↪ 1 banana prata cortada em fatias congelada;\n↪ 10 a 15 morangos congelados;\n↪ 1 colher de aveia; \n↪ 1 iogurte zero lactose 100g.',
        '↪ Bata no liquidificador ou processador  o iogurte e a banana;\n\n↪ Acrescente os morangos e a aveia e bata por mais alguns minutos;\n\n↪ Sirva.',
        'TelaComFrutasMorango');
  }
}
