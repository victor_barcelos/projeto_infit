import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaPanquecaComMorango());

class ReceitaPanquecaComMorango extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Panqueca e morango',
        '↪ 1 xícara de farinha de trigo integral;\n↪ 1 xícara de leite;\n↪ 1 ovo;\n↪ 1  colher de sopa de açúcar mascavo (opcional);\n↪ Morangos frescos; \n↪ Mel.',
        '↪ Misture em uma tigela a farinha, o leite, o ovo, o óleo e, se quiser, o açúcar. O ideal é obter uma massa sem grumos e meio líquida;\n\n↪ Frite uma porção da panqueca numa frigideira antiaderente Vire a panqueca ao contrário quando estiver borbulhando, e deixe dourar do outro lado.\n\n↪ Retire a panqueca quando estiver dourada dos dois lados, e repita com a massa restante;\n\n↪ Quando as panquecas estiverem prontas, resta servir com morangos frescos lavados e inteiros ou cortados em pedaços, e mel. ',
        'TelaComFrutasMorango');
  }
}
