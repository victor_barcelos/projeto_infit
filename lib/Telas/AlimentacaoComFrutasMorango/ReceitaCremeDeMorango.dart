import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaCremeDeMorango());

class ReceitaCremeDeMorango extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Creme de morango',
        '↪ 1/2 xícara de aveia em flocos;\n↪ 2 xícaras de morango;\n↪ 1/2 xícara de leite da sua preferência.',
        '↪ Bata todos os ingredientes no liquidificador;\n\n↪ Leve ao freezer por no mínimo duas horas;\n\n↪ Sirva.',
        'TelaComFrutasMorango');
  }
}
