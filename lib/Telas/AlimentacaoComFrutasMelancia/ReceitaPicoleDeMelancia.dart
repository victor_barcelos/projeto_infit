import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaPicoleDeMelancia());

class ReceitaPicoleDeMelancia extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Picolé de melancia',
        '↪ 1 kg de polpa de melancia cortada grosseiramente;\n↪ 150 g de açúcar em pó;\n↪ 50 g de xarope de glucose',
        '↪ Triture a melancia. Retirar parte do sumo (o mínimo possível) e levá-la ao fogo com a glucose e o açúcar, até que este esteja completamente dissolvido;\n\n↪ Deixe a mistura esfriar e adicione o sumo restante;\n\n↪  Encha os moldes de sorvete e congele.',
        'TelaComFrutasMelancia');
  }
}
