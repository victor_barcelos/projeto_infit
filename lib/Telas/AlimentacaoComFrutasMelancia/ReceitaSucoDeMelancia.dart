import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaSucoDeMelancia());

class ReceitaSucoDeMelancia extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Suco rosa',
        '↪ 2 fatias grossas de melancia com semente;\n↪ 3 beterrabas pequenas orgânicas (com casca);\n↪ 1 punhado de morangos congelados ou frescos (1 mão cheia);\n↪ 2 colheres de sobremesa de chia;\n↪ 1 colher de sobremesa de linhaça dourada; \n↪ 600 ml de água de coco.',
        '↪ Pique grosseiramente a melancia e coloque no liquidificador junto com os outros ingredientes. Bata bem;\n\n↪ Coe, caso ache necessário. Sirva com bastante gelo. Uma opção é usar água filtrada e fazer gelinhos com a água de coco, se preferir. Não é necessário colocar açúcar, a não ser que você queira. ',
        'TelaComFrutasMelancia');
  }
}
