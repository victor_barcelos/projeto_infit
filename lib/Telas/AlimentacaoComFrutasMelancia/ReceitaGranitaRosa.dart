import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaGranitaRosa());

class ReceitaGranitaRosa extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Granita rosa',
        '↪ 1 kg de melancia cortada em pedaços;\n↪ 100g de açúcar orgânico;\n↪ Suco de um limão;\n↪ Suco de seis laranjas;\n↪ Folhas de hortelã para decorar.',
        '↪ Corte a melancia em pedaços e descarte a casca e a parte branca. Passe-a por uma peneira para tirar as sementes e recolha o purê (suco) numa tigela;\n\n↪ À parte, coloque numa panela os sucos de limão e de laranja, acrescente o açúcar e leve ao fogo baixo para fazer uma calda. Deixe ferver por uns cinco minutos;\n\n↪ Quando a calda de laranja esfriar junte-a ao suco de melancia misture bem e coloque num recipiente para ir ao freezer por pelo menos por três horas; \n\n↪ Detalhe: coloque numa forma razoavelmente rasa. Quanto mais fina a camada de líquido mais rápido vai congelar e facilitará para raspar; \n\n↪ Depois de congelado retire do freezer e raspe a granita com um garfo. Distribua-a em taças, decore com as folhas de hortelã e refresque-se.',
        'TelaComFrutasMelancia');
  }
}
