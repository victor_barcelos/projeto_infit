import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

class MeuApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(corFundo),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.only(top: 20, right: 40, left: 40, bottom: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                iconeMenu(context, corSuporteFundoVerde, corSuporteFundo, Alignment.topLeft),
                caixaDeTexto('Alimentação', corSuporteTexto, tamanhoLetraTitulos, TextAlign.center),
                pularLinha(10),
                //caixaDeTexto('O que você quer cozinhar hoje?', corSuporteTexto, tamanhoLetraSubtitulos, TextAlign.center),
                Text(
                  'O que você quer cozinhar hoje?',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.gothicA1().copyWith(
                    color: Color(corSuporteTexto),
                    fontSize: tamanhoLetraSubtitulos,
                    fontWeight: FontWeight.bold),
                ),
                pularLinha(20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    botaoCorEstado('comFrutas', 'Com frutas', context, 'assets/imagens/alimentacao/fruit_foods_2.jpeg'),
                    botaoCorEstado('salgadas', 'Salgadas', context, 'assets/imagens/alimentacao/salty_food_2.jpg'),
                  ],
                ), pularLinha(20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    botaoCorEstado('Bebidas', 'Bebidas', context, 'assets/imagens/alimentacao/food_smoothie_2.jpeg'),
                    botaoCorEstado('TelaVeganas', 'Veganas', context, 'assets/imagens/alimentacao/vegan_food.jpeg'),
                  ],
                ), pularLinha(20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    botaoCorEstado('TelaLowCarb', 'Low Carb', context, 'assets/imagens/alimentacao/vegan_food_2.jpeg'),
                    botaoCorEstado('TelaDoces', 'Doces', context, 'assets/imagens/alimentacao/sweet_food.jpeg'),
                  ],
                ), pularLinha(20),
                botaoSemBorda('Retornar', 'menu', corSuporteTexto, context),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
