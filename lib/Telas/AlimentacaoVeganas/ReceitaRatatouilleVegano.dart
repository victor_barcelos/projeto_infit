import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaRatatouilleVegano());

class ReceitaRatatouilleVegano extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Ratatouille vegano',
        '↪ 2 abobrinhas;\n↪ 6 tomates médios; \n↪ 2 berinjelas;\n↪ Molho de tomate caseiro;\n↪ Temperos a gosto.',
        '↪ Cortar as abobrinhas, os tomates e as berinjelas em rodelas;\n\n↪ Untar uma forma redonda (assadeira) com azeite de oliva; \n\n↪ Espalhar metade do molho de tomate na assadeira;\n\n↪ Distribuir as rodelas por sobre a assadeira intercalando-as (1 de berinjela, 1 de abobrinha, 1 de tomate), preenchendo toda a assadeira;\n\n↪ Cobrir tudo com a metade restante do molho de tomate;\n\n↪ Salpicar os temperos escolhidos;\n\n↪ Levar ao forno preaquecido em temperatura média (180ºC) por cerca de 30 minutos (sempre olhando para garantir que não queime);\n\n↪ Retirar e servir.',
        'TelaVeganas');
  }
}
