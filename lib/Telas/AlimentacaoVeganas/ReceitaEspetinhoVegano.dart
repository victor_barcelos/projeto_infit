import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaEspetinhoVegano());

class ReceitaEspetinhoVegano extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Espetinho vegano',
        '↪ Tofu;\n↪ Cogumelos; \n↪ Carne e salsicha de soja;\n↪ Berinjela cortada em cubos;\n↪ Cebola corta ao meio ou inteira com casca;\n↪ Pimentões recheados queijo;\n↪ Cenoura em cubos grandes;\n↪ Couve-flor;\n↪ Abobrinha;\n↪ Brócolis;\n↪ Vagem; \n↪ Espiga de milho;\n↪ Tomate sem semente;\n↪ Frutas como maçã, abacaxi e pêssego.',
        '↪ Assar o tofu, os cogumelos e a carne de soja na churrasqueira. Todos os legumes também podem ser assados, especialmente o pimentão recheado com queijo, que irá derreter com o calor; \n\n↪ Além disso, os legumes podem ser consumidos crus na forma de salada, e pode-se usar o pão de alho para acompanhar as carnes veganas; \n\n↪ Monte os espetinhos e sirva.',
        'TelaVeganas');
  }
}
