import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaBoloDeChocolateVegano());

class ReceitaBoloDeChocolateVegano extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Bolo de chocolate',
        '↪ 200 g de tâmaras secas sem caroços;\n↪ 2 xícaras de farinha de trigo; \n↪ 3 colheres de sopa de cacau cru;\n↪ 1 colher de sopa de fermento em pó;\n↪ 1 colher de chá de bicarbonato de sódio;\n↪ 1 ½ xícaras de leite vegetal;\n↪ 4 colheres de sopa de óleo de coco;\n↪ 1 colher de chá de suco de limão;\n↪ Cobertura: 1 colher de sopa de amido de milho; 7 colheres de chá de cacau; 1 xícara de leite de amêndoas.',
        '↪ Massa: triturar as tâmaras em processador, em seguida, misturar todos os ingredientes com um fuê. Assar em forno pré aquecido a 180 °C por 30 minutos. \n\n↪ Cobertura: Dissolver amido de milho no leite vegetal frio, mexendo com fuê, misturar com o cacau e deixar ferver por 5 minutos. Após mornar, servir em cima do bolo.',
        'TelaVeganas');
  }
}
