import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaPanquecaDeBanana());

class ReceitaPanquecaDeBanana extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Panqueca de banana ',
        '↪ 1 banana;\n↪ 2 ovos;\n↪ 2 colheres de sopa de aveia;\n↪ 1 pitada de canela e mel a gosto;\n↪ Óleo para untar.',
        '↪ Coloque a banana em um prato fundo e amasse-a com o auxílio de um garfo;\n\n↪ Adicione os ovos, a aveia e a canela e misture até incorporar tudo;\n\n↪ Unte uma frigideira com óleo e espalhe a massa da panqueca. Deixe cozinhar com tampa até dourar embaixo, depois vire;\n\n↪ Transfira para um prato e sirva com mel.',
        'comFrutasBanana');
  }
}
