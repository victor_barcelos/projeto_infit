import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaBoloDeBanana());

class ReceitaBoloDeBanana extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        'Bolo de banana ',
        '↪ 3 ovos;\n↪ 4 bananas;\n↪ 1 xícara de uva passa;↪ 1/2 xícara de farinha de aveia\n↪ 1 colher de fermento;\n↪ Canela;\n↪ Óleo de coco.',
        '↪ No liquidificador, adicione os ovos, as bananas, a canela e a uva-passa. Bata por 30 segundos;\n\n↪ Adicione a farinha de aveia e o fermento, depois bata por mais 1 minuto e reserve;\n\n↪ Unte uma forma redonda com óleo de coco e farinha de aveia. Despeje a massa dentro e espalhe; \n\n↪ Leve ao forno a 200 graus por 40 minutos ou até assar por completo. Após esfriar, retire da forma e sirva.',
        'comFrutasBanana');
  }
}
