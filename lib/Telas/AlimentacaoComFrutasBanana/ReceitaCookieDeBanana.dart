import 'package:flutter/material.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

void main() => runApp(ReceitaCookieDeBanana());

class ReceitaCookieDeBanana extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return layoutReceitas(
        context,
        ' Cookie de banana ',
        '↪ 1 ovo batido;\n↪ 1 colher de sopa de mel\n↪ 1 banana amassada;\n↪ 1 colher de chá de manteiga\n↪ 1/4 de xícara de aveia em flocos;\n↪ 1 e 1/4 de xícara de farinha de aveia;\n↪ 2 colheres de sobremesa de fermento em pó;\n↪ Canela.',
        '↪ Em uma tigela, adicione o ovo e o mel e misture com um fouet. Acrescente a banana e a manteiga, depois mexa novamente;\n\n↪ Misturando após adicionar cada ingrediente, coloque a aveia, a canela e um pouco da farinha de aveia. Adicione o fermento, misture e acrescente o restante da farinha, mexendo até formar uma massinha que solte das mãos;\n\n↪ Polvilhe farinha de aveia em uma bancada e abra a massa com um rolo, até ficar com uma espessura aproximada de 0,5 centímetro. Polvilhe mais canela por cima da massa; \n\n↪ Use um cortador redondo para modelar a massa em biscoitinhos e coloque-os em uma assadeira;\n\n↪ Com um garfo, faça furinhos na massa e leve para assar em forno preaquecido a 180 graus por cerca de 10 a 12 minutos;\n\n↪ Após dourar, retire do forno, coloque em um prato e está pronto para servir.',
        'comFrutasBanana');
  }
}
