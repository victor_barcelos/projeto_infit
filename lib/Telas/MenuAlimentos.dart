import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
//import 'package:testeteladelogin/Model/metodosTela.dart';
import 'package:testeteladelogin/metodosTelasAlimentacao.dart';

class MenuAlimentos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(corFundo),
        body: SingleChildScrollView(
          child: SafeArea(
            child: Container(
              child: Column(
                children: [
                  pularLinha(40),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Do que você precisa?',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.getFont(fonte,
                          color: Color(corSuporteTexto),
                          fontSize: tamanhoLetraTitulos,
                          fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          pularLinha(40),
                          botaoCorEstado('comFrutas', 'Ver receitas salvas', context, 'assets/imagens/gerais/fouet.jpg'),
                          pularLinha(40),
                          botaoCorEstado('TelaNutricionistas', 'Indicação de nutricionistas', context, 'assets/imagens/gerais/nutricionistas.jpg'),
                          pularLinha(40),
                          botaoSemBorda('Retornar', 'alimentacao', corSuporteTexto, context),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      );
  }
}
